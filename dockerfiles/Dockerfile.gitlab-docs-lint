############################
#
# Image the contains the dependencies to run the lints.
# It downloads the gitlab-docs repository based on the
# branch the Docker image is invoked from.
# Based on Alpine.
#
############################

FROM registry.gitlab.com/gitlab-org/gitlab-docs:base
MAINTAINER GitLab Technical Writing team

# VALE_VERSION and MARKDOWNLINT_VERSION are defined in .gitlab-ci.yml
ARG VALE_VERSION
ARG MARKDOWNLINT_VERSION
ENV GOBIN=/usr/local/bin

# Install vale
RUN curl -L https://github.com/errata-ai/vale/archive/v${VALE_VERSION}.tar.gz -o v${VALE_VERSION}.tar.gz && \
    tar -xvzf v${VALE_VERSION}.tar.gz && \
    cd vale-${VALE_VERSION} && \
    os=linux exe=vale make && \
    make install && \
    cd .. && \
    rm -r vale-${VALE_VERSION} && \
    rm v${VALE_VERSION}.tar.gz && \
    apk del go

# Set up needed environment variables that are called with --build-arg when
# the Docker image is built (see .gitlab-ci.yml).
ARG CI_COMMIT_REF_NAME
# If CI_COMMIT_REF_NAME is not set (local development), set it to master
ENV CI_COMMIT_REF_NAME ${CI_COMMIT_REF_NAME:-master}

WORKDIR /tmp

# markdownlint-cli pinned to control when new versions are put in place.
RUN yarn global add markdownlint-cli@${MARKDOWNLINT_VERSION} && yarn cache clean

RUN wget https://gitlab.com/gitlab-org/gitlab-docs/-/archive/$CI_COMMIT_REF_NAME/gitlab-docs-$CI_COMMIT_REF_NAME.tar.bz2 \
  && tar xvjf gitlab-docs-$CI_COMMIT_REF_NAME.tar.bz2 \
  && mv gitlab-docs-$CI_COMMIT_REF_NAME gitlab-docs \
  && rm gitlab-docs-$CI_COMMIT_REF_NAME.tar.bz2

RUN cd gitlab-docs \
  && yarn install --frozen-lockfile \
  && bundle install --jobs 4
